import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AllFruits extends StatefulWidget {
  const AllFruits({Key? key}) : super(key: key);

  @override
  _AllFruitsState createState() => _AllFruitsState();
}

class _AllFruitsState extends State<AllFruits> {
  // text fields' controllers
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  // Create a CollectionReference called _products that references the firestore collection
  final CollectionReference _products =
      FirebaseFirestore.instance.collection('products');

  // This function is triggered when the floatting button or one of the edit buttons is pressed
  // Adding a product if no documentSnapshot is passed
  // If documentSnapshot != null then update an existing product

  Future<void> _createOrUpdate([DocumentSnapshot? documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      _nameController.text = documentSnapshot['name'];
      _priceController.text = documentSnapshot['price'].toString();
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(top: 20, left: 20, right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextFormField(
                    controller: _nameController,
                    decoration: const InputDecoration(labelText: 'ชื่อสินค้า'),
                    style: TextStyle(fontFamily: "IBMSUB",fontWeight: FontWeight.bold),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'กรุณาใส่ชื่อสินค้า';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    keyboardType:
                        const TextInputType.numberWithOptions(decimal: true),
                    controller: _priceController,
                    decoration: const InputDecoration(
                      labelText: 'ราคาสินค้า (฿)',
                    ),
                    style: TextStyle(fontFamily: "IBMSUB",fontWeight: FontWeight.bold),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'กรุณาใส่ราคาสินค้า';
                      }
                      return null;
                    },
                  ),

                  const SizedBox(
                    height: 20,
                  ),

                  ElevatedButton(
                    child: Text(action == 'create' ? 'บันทึกสินค้า' : 'อัพเดตสินค้า',style: TextStyle(fontFamily: "IBM"),),
                    onPressed: () async {
                      final String name = _nameController.text;
                      final double? price =
                          double.tryParse(_priceController.text);
                      if (name != null && price != null) {
                        if (action == 'create') {
                          // Persist a new product to Firestore
                          await _products
                              .add({"name": name, "price": price})
                              .then((value) => print("Product Added"))
                              .catchError((error) =>
                                  print("Failed to add product: $error"));
                        }

                        if (action == 'update') {
                          // Update the product
                          await _products
                              .doc(documentSnapshot!.id)
                              .update({"name": name, "price": price})
                              .then((value) => print("Products Updated"))
                              .catchError((error) =>
                                  print("Failed to update this products: $error"));
                        }

                        // Clear the text fields
                        _nameController.text = '';
                        _priceController.text = '';

                        // Hide the bottom sheet
                        Navigator.of(context).pop();
                      }
                    },
                  )
                ],
              ),
            ),
          );
        });
  }

  // Deleting a product by id
  Future<void> _deleteProduct(String productId) async {
    await _products
        .doc(productId)
        .delete()
        .then((value) => print("Products Deleted"))
        .catchError((error) => print("Failed to delete this products: $error"));

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('คุณได้ลบสินค้าสำเร็จแล้ว')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(0, 43, 91,1),
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(0, 43, 91,1),
        title: const Text('สินค้าของฉัน',style: TextStyle(fontFamily: 'IBM'),),
      ),
      // Using StreamBuilder to display all products from Firestore in real-time
      body: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: StreamBuilder(
          stream: _products.snapshots(),
          builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
            if (streamSnapshot.hasData) {
              return ListView.builder(
                itemCount: streamSnapshot.data!.docs.length,
                itemBuilder: (context, index) {
                  final DocumentSnapshot documentSnapshot =
                      streamSnapshot.data!.docs[index];
                  return Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(40)),
                    ),
                    margin: const EdgeInsets.only(top: 15,left: 15,right: 15,bottom: 5),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: ListTile(
                        title: Padding(
                          padding: const EdgeInsets.only(bottom: 1),
                          child: Text(
                            documentSnapshot['name'],
                            style: TextStyle(
                              fontSize: 18,
                                fontFamily: "IBM",
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        subtitle: Text(
                          '฿'+ documentSnapshot['price'].toString(),
                          style: TextStyle(
                              fontSize: 15,
                              fontFamily: "IBMSUB",
                              fontWeight: FontWeight.bold),
                        ),
                        trailing: SizedBox(
                          width: 100,
                          child: Row(
                            children: [
                              // Press this button to edit a single product
                              IconButton(
                                  icon: const Icon(
                                    Icons.edit,
                                    color: Color.fromRGBO(
                                        8, 108, 220, 0.8156862745098039),
                                  ),
                                  onPressed: () =>
                                      _createOrUpdate(documentSnapshot)),
                              // This icon button is used to delete a single product
                              IconButton(
                                  icon: const Icon(
                                    Icons.delete,
                                    color: Color.fromRGBO(229, 55, 55, 1.0),
                                  ),
                                  onPressed: () =>
                                      _deleteProduct(documentSnapshot.id)),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              );
            }

            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
      // Add new product
      floatingActionButton: FloatingActionButton(
        onPressed: () => _createOrUpdate(),
        child: const Icon(Icons.add,color: Colors.black54,),
        backgroundColor:Color.fromRGBO(143, 227, 207,1),
      ),
    );
  }
}
