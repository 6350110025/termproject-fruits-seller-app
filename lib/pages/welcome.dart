import 'package:flutter/material.dart';
import 'package:fruits_seller_mobile/pages/login.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Stack(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Color.fromRGBO(0, 43, 91,1),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 260,
                    child: Image.asset("images/fs_welcome.gif"),
                  ),
                  const SizedBox(
                    height: 420,
                  )
                ],
              ),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  SizedBox(
                    height: 145,
                  ),
                  Text("START FOR SELE",
                    style: TextStyle(
                      fontSize: 12.0,
                      fontFamily: "Inter",
                      fontWeight: FontWeight.normal,
                      color: Color.fromRGBO(143, 227, 207,1),
                      letterSpacing: 7.0,
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text("ขายผลไม้สดกับ\nFruits Seller",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontFamily: "IBM",
                      fontSize: 45.0,
                      letterSpacing: 1.0,
                    ),
                  ),

                  SizedBox(
                    height: 10.0,
                  ),
                  Text("หากคุณมีผลไม้สดใหม่จากสวน\n"
                      "นำผลไม้ของคุณมาขายกับ Fruits Seller\n"
                      "ดูแลคุณจนนำไปสู่การขายที่สร้างรายได้.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white30,
                      fontWeight: FontWeight.bold,
                      fontFamily: "IBM",
                      fontSize: 15.0,
                      letterSpacing: 0.5,
                    ),
                  ),
                ],
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(bottom: 55.0),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: SizedBox(
                  width: 320,
                  height: 60,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> Login()));
                    },
                    style: ElevatedButton.styleFrom(
                        primary: const Color.fromRGBO(143, 227, 207,1),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        )
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: const [
                        Text("ขายกับเราตอนนี้",
                          style: TextStyle(
                            color: Color(0xFF000000),
                            fontSize: 18.5,
                            fontFamily: "IBM",
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Icon( // <-- Icon
                          Icons.arrow_forward_ios,
                          size: 18.0,
                          color: Color(0xFF2C2C2C),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),

    );
  }
}
