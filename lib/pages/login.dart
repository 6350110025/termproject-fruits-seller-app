import 'package:flutter/material.dart';
import 'package:fruits_seller_mobile/Auth/authentication.dart';
import 'package:fruits_seller_mobile/pages/home_screen.dart';
import 'package:fruits_seller_mobile/pages/signup.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.all(8.0),
        children: <Widget>[
          SizedBox(height: 50),
          // logo
          Column(
            children: [
              Image(
                image: NetworkImage(
                    'https://media1.giphy.com/media/gHPOb1fEVWu5GHL2tk/giphy.gif?cid=ecf05e47w9djvwy13h5h97gs0hapyqucfrjg7z7bbo8exc36&rid=giphy.gif&ct=g'),
                height: 150,
              ),
              SizedBox(height: 18),
              Text(
                'ขอต้อนรับสู่ Fruits Seller',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  fontFamily: "IBM",
                  color: Color.fromRGBO(0, 43, 91, 1),
                ),
              ),
              Text(
                'กรุณาเข้าสู่ระบบก่อนเริ่มใช้งาน',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontFamily: "IBMSUB",
                ),
              )
            ],
          ),

          SizedBox(
            height: 30,
          ),

          Padding(
            padding: const EdgeInsets.all(16.0),
            child: LoginForm(),
          ),

          Center(
            child: Text(
              '- หรือเข้าสู่ระบบโดย -',
              style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
              fontFamily: "IBMSUB",
            ),
            ),
          ),

      SizedBox(height: 20),

      Padding(
        padding: const EdgeInsets.only(left: 160,right: 160),
        child: SizedBox(
          height: 50,
          child: ElevatedButton(
            onPressed: () {
              AuthenticationHelper().signInWithGoogle().then((result) {
                if (result == null) {
                  Navigator.pushReplacement(
                      context, MaterialPageRoute(builder: (context) => HomeScreen()));
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(
                      result,
                      style: TextStyle(fontSize: 16),
                    ),
                  ));
                }
              });
            },
            style: ElevatedButton.styleFrom(
                primary: Color.fromARGB(255, 230, 255, 253),
                elevation: 4,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(40)))),
            child: Image.asset("images/Logo-google-icon-PNG.png",scale: 20,),
          ),
        ),
      ),

          SizedBox(height: 20),

          Row(
            children: <Widget>[
              SizedBox(width: 30),
              Text('หากคุณยังยังไม่มีบัญชี ? ',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                    fontFamily: "IBMSUB",
                  )),
              GestureDetector(
                onTap: () {
                  // Navigator.pushNamed(context, '/signup');
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Signup()));
                },
                child: Text('ลงทะเบียนเลยตอนนี้',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.blue,
                      fontFamily: "IBM",
                    )),
              )
            ],
          ),
        ],
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  LoginForm({Key? key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  String? email;
  String? password;

  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          // email
          TextFormField(
            // initialValue: 'Input text',
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.email_outlined),
              labelText: 'Email',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  const Radius.circular(100.0),
                ),
              ),
            ),
            validator: (value) {
              if (value!.isEmpty) {
                return 'กรุณากรอกอีเมลที่ถูกต้อง';
              }
              return null;
            },
            onSaved: (val) {
              email = val;
            },
          ),
          SizedBox(
            height: 20,
          ),

          // password
          TextFormField(
            // initialValue: 'Input text',
            decoration: InputDecoration(
              labelText: 'Password',
              prefixIcon: Icon(Icons.lock_outline),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  const Radius.circular(100.0),
                ),
              ),
              suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    _obscureText = !_obscureText;
                  });
                },
                child: Icon(
                  _obscureText ? Icons.visibility_off : Icons.visibility,
                ),
              ),
            ),
            obscureText: _obscureText,
            onSaved: (val) {
              password = val;
            },
            validator: (value) {
              if (value!.isEmpty) {
                return 'รหัสผ่านไม่ถูกต้อง กรุณากรอกรหัสผ่านใหม่อีกครั้ง';
              }
              return null;
            },
          ),

          SizedBox(height: 30),

          SizedBox(
            height: 54,
            width: 184,
            child: ElevatedButton(
              onPressed: () {
                // Respond to button press

                if (_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();

                  AuthenticationHelper()
                      .signIn(email: email!, password: password!)
                      .then((result) {
                    if (result == null) {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) => HomeScreen()));
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                          result,
                          style: TextStyle(fontSize: 16),
                        ),
                      ));
                    }
                  });
                }
              },
              style: ElevatedButton.styleFrom(
                primary: Color.fromRGBO(0, 43, 91,1),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(24.0)))),
              child: Text(
                'Login',
                style: TextStyle(fontSize: 24,fontFamily: 'IBM'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
